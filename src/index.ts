import createMiddleware, { ReduxWamp } from './middleware';
import ReduxWampCaller from './roles/call';
import ReduxWampSubscriber from './roles/subscriber';
import ReduxWampConnection from './roles/connection';

export { ReduxWampCaller, ReduxWampSubscriber, ReduxWampConnection, ReduxWamp };
export default createMiddleware;
