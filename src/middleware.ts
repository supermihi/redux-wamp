import autobahn, { Session } from 'autobahn';
import { Action, Dispatch, Middleware, MiddlewareAPI } from 'redux';
import merge from 'lodash.merge';
import * as actions from './actions';

const setDeferred = (action: actions.WampAction) => merge(action, { meta: { deferred: true } });

const setError = (action: actions.WampAction, error: any): actions.WampAction => (
  setDeferred(merge(action, { payload: error, error: true }))
);

const setResult = <TPay>(action: actions.WampAction<TPay>, payload: TPay, stage: string) => (
  setDeferred(merge(action, { payload, meta: { stage } }))
);

export class ReduxWamp {
  session: autobahn.Session;
  private connection: autobahn.Connection;
  private subscriptions: Map<string, autobahn.ISubscription>;

  constructor() {
    this.session = null;
    this.connection = null;
    this.subscriptions = new Map();
  }

  createMiddleware(): Middleware {
    return <S>(api: MiddlewareAPI<S>) => next => <A extends Action>(action: A) =>
      this.dispatch(api, next, action);
  }

  private dispatch<S, A extends Action>({ dispatch, getState }: MiddlewareAPI<S>, next: Dispatch<S>, action: A) {
    if (actions.isWampAction(action) && !action.meta.deferred) {
      if (actions.isConnectionAction(action)) {
        this.handleConnection(action, dispatch);
      } else if (actions.isCallerAction(action)) {
        this.handleCaller(action, dispatch);
      } else if (actions.isSubscriberAction(action)) {
        this.handleSubscriber(action, dispatch);
      }
    }
    return next(action);
  }

  private async handleCaller<S>(action: actions.CallerAction<any, any>, dispatch: Dispatch<S>) {
    const { uri, args, stage } = action.meta;
    try {
      const answer = await this.session.call(uri, args);
      dispatch(setResult(action, answer, actions.CallerStage.result));
    } catch (error) {
      dispatch(setError(action, error instanceof autobahn.Error
        ? `${error.error}: ${error.args.join(', ')}`
        : (<Error>error).message));
    }
  }

  private connect<S>(action: actions.ConnectionAction, dispatch: Dispatch<S>) {
    const { meta: { url, realm } } = action;
    if (this.session && this.session.isOpen) {
      return dispatch(setError(action, 'already connected'));
    }
    this.connection = new autobahn.Connection({ url, realm });
    this.connection.onopen = (session) => {
      this.session = session;
      dispatch(setResult(action, session, actions.ConnectionStage.connected));
    };
    this.connection.onclose = (reason, details) => {
      this.session = null;
      if (reason === 'unreachable') {
        dispatch(setError(action, 'connection failure: unreachable'));
        return true;
      }
      // TODO: error handling
      dispatch(setResult(action, reason, actions.ConnectionStage.disconnected));
      return false; // on lost: try to reconnect
    };
    this.connection.open();
  }

  private disconnect<S>(action: actions.ConnectionAction, dispatch: Dispatch<S>) {
    if (!this.session || !this.session.isOpen) {
      return dispatch(setError(action, 'not connected'));
    }
    this.connection.close();
  }

  private handleConnection<S>(action: actions.ConnectionAction, dispatch: Dispatch<S>) {
    const stage = action.meta.stage;
    const { connect, disconnect } = actions.ConnectionStage;
    switch (stage) {
      case connect:
        return this.connect(action, dispatch);
      case disconnect:
        return this.disconnect(action, dispatch);
      default:
        throw `invalid connection stage: ${stage}`;
    }

  }

  private async handleSubscriber<S>(action: actions.SubscriberAction<any>, dispatch: Dispatch<S>) {

    const { meta: { uri, stage } } = action;
    const { subscribe, unsubscribe, receive } = actions.SubscriberStage;
    switch (stage) {
      case subscribe:
        try {
          const subscription = await this.session.subscribe(uri, (args) => {
            dispatch(setResult(action, args, receive));
          });
          this.subscriptions.set(uri, subscription);
        } catch (error) {
          dispatch(setError(action, (<Error>error).message));
        }
      case unsubscribe:
        const subscription = this.subscriptions.get(uri);
        if (subscription) {
          try {
            await this.session.unsubscribe(subscription);
          } catch (error) {
            dispatch(setError(action, error));
          }
        } else {
          dispatch(setError(action, `not subscribed to ${uri}`));
        }
      default:
        throw `unknown subscriber stage: ${stage}`;
    }
  }
}

export default function createReduxMiddleware() {
  const reduxWamp = new ReduxWamp();
  return reduxWamp.createMiddleware();
}
