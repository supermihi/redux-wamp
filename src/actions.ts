import { Action } from 'redux';

export interface ActionMeta<Payload, Meta> extends Action {
  payload: Payload;
  meta: Meta;
  error?: any;
}

/** Describes the type of WAMP service of an action. */
export enum Role {
  /** Manage connection to a WAMP router */
  connection = 'connection',
  /** Caller role */
  caller = 'caller',
  /** Subscriber role (subscribe, receive, unsubscribe) */
  subscriber = 'subscriber',
}

/** the "meta.wamp" part of a WAMP redux action. */
export interface WampMeta extends Object {
  role: Role;
  stage: string;
  deferred?: boolean; // whether this action was deferredly created by the WAMP middleware instead of user-generated
}

/** A redux WAMP action.
 *  @template Payload the payload type (meaning depends on service type)
 *  @template Meta type of the "meta" property
 */
export type WampAction<Payload = any, Meta extends WampMeta = WampMeta> = ActionMeta<Payload, Meta>;

export function isWampAction(action: any): action is WampAction {
  return action.meta && action.meta.role && action.meta.stage;
}


export enum ConnectionStage {
  connect = 'connect', // connection requested
  connected = 'connected', // connection established
  disconnect = 'disconnect', // disconnection request
  disconnected = 'disconnected', // disconnected
}

/** Meta of a connect or disconnect action */
export interface ConnectionMeta extends WampMeta {
  role: Role.connection;
  stage: ConnectionStage;
  /** Url of the WAMP router */
  url: string;
  /** Realm to join */
  realm: string;
}

export type ConnectionAction = WampAction<null, ConnectionMeta>;

export function isConnectionAction(action: WampAction): action is ConnectionAction {
  return action.meta.role === Role.connection;
}

export enum CallerStage {
  call = 'call', // remote method called
  result = 'result', // result of a remote method call received
}
/** Meta of a caller role action */
export interface CallerMeta<T extends any[]> extends WampMeta {
  role: Role.caller;
  stage: CallerStage;
  /** Uri of the callee */
  uri: string;
  /** Call arguments */
  args: T;
}
/** A caller action.
 *  @template Result type of the callee's result
 *  @template Args type of the callee's arguments
 */
export type CallerAction<Result, Args extends any[]> = WampAction<Result, CallerMeta<Args>>;

export function isCallerAction(action: WampAction): action is CallerAction<any, any> {
  return action.meta.role === Role.caller;
}

export enum SubscriberStage {
  subscribe = 'subscribe', // subscription requested
  receive = 'receive', // message received
  unsubscribe = 'unsubscribe', // unsubscription requested
}

/** Meta of a subscription action */
export interface SubscriberMeta extends WampMeta {
  role: Role.subscriber;
  stage: SubscriberStage;
  /** The topic's URI */
  uri: string;
}

export type SubscriberAction<Params extends any[]> = WampAction<Params, SubscriberMeta>;
export function isSubscriberAction(action: WampAction): action is SubscriberAction<any> {
  return action.meta.role === Role.subscriber;
}






