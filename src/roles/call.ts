import { CallerAction, Role, CallerMeta, CallerStage } from '../actions';
import { WampReducer } from './index';
import { Action, Reducer } from 'redux';
const { call, result } = CallerStage;

export interface ReducerMap<S, Result, Args> {
  onResult: (state: S, args: Args, response: Result) => S;
  onCall?: (state: S, args: Args) => S;
  onError?: (state: S, args: Args, error: any) => S;
}

/** Helper class for managing a WAMP caller in redux.
 *  @template Result The called method's result type
 *  @template Args The called method's arguments
 */
export default class ReduxWampCaller<Result, Args extends any[]= null>  {
  readonly uri: string;
  readonly type: string;
  /** Action creator for a call action. */
  readonly createCallAction: (args?: Args) => CallerAction<Result, Args>;

  constructor(type: string, uri: string) {
    this.type = type;
    this.uri = uri;
    this.createCallAction = (args: Args = null) => ({
      type,
      payload: null,
      meta: {
        args,
        uri,
        stage: call,
        role: Role.caller,
      },
    });
  }

  isCallAction(action: Action): action is CallerAction<Result, Args> {
    return action.type === this.type;
  }

  /** Helper for creating a set of redux reducer for a call action.
   *  @param onResult reducer applied when the result of a call arrives
   *  @param onCall reducer applied when the call is requested.
   *  @param onError reducer applied when a call results in an error.
   */
  createReducer<S>(
    onResult: (state: S, args: Args, response: Result) => S,
    onCall?: (state: S, args: Args) => S,
    onError?: (state: S, args: Args, error: any) => S)
    : WampReducer<S, Result, CallerMeta<Args>> {
    return (state: S, action: CallerAction<Result, Args>) => {
      const { error, payload, meta: { args, stage } } = action;
      if (error) {
        return onError ? onError(state, args, error) : state;
      }
      if (stage === result) {
        return onResult(state, args, payload);
      }
      return onCall ? onCall(state, args) : state;
    };
  }

  /** Reducer */
  handleAction<S>(reducers: ReducerMap<S, Result, Args>, initialState: S = undefined): Reducer<S> {
    const { onCall, onResult, onError } = reducers;
    const wampReducer = this.createReducer(onResult, onCall, onError);

    return (state: S = initialState, action: Action) => action.type === this.type
      ? wampReducer(state, action as CallerAction<Result, Args>)
      : state;
  }
}
