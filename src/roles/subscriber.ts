import { SubscriberAction, Role, SubscriberMeta, SubscriberStage } from '../actions';
import { WampReducer } from './index';
const { subscribe, receive, unsubscribe } = SubscriberStage;

/** Helper class for managing a WAMP subscription to a specific topic in a redux app. */
export default class ReduxWampSubscriber<Params extends any[]> {
  type: string;
  uri: string;
  /** Create a @see ReduxWampSubscriber.
   *  @param type The redux action type
   *  @param uri The topic's URI
   */
  constructor(type: string, uri: string) {
    this.type = type;
    this.uri = uri;
  }

  /** Action creator subscribing to a topic. */
  createSubscribeAction() : SubscriberAction<Params> {
    const { uri, type } = this;
    return {
      type,
      payload: null,
      meta: { uri, role: Role.subscriber, stage: subscribe },
    };
  }

  /** Action creator for unsubscribing from a topic. */
  createUnsubscribeAction(): SubscriberAction<null> {
    const { uri, type } = this;
    return {
      type,
      payload: null,
      meta: { uri, role: Role.subscriber, stage: unsubscribe },
    };
  }

  /** Helper for creating a set of redux reducers for a WAMP topic subscription.
   *  @param onMessage reducer applied when a message is posted on the subscribed topic
   *  @param onSubscribe reducer applied on subscription
   *  @param onUnsubscribe reducer applied on unsubscription
   *  @param onError reducer applied when an error occurs
   */
  createReducer<S>(
    onMessage: (state: S, args: Params) => S,
    onSubscribe?: (state: S) => S,
    onUnsubscribe?: (state: S) => S,
    onError?: (state: S, error: any) => S)
    : WampReducer<S, Params, SubscriberMeta> {
    return (state, action) => {
      const { error, payload, meta: { role, stage } } = action;
      if (error) {
        return onError ? onError(state, error) : state;
      }
      switch (stage) {
        case receive:
          return onMessage(state, payload);
        case subscribe:
          return onSubscribe ? onSubscribe(state) : state;
        case unsubscribe:
          return onUnsubscribe ? onUnsubscribe(state) : state;
      }
      return state;
    };
  }
}
