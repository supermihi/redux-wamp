import autobahn from 'autobahn';
import { Reducer, Action } from 'redux';

import { ConnectionAction, Role, ConnectionMeta, ConnectionStage } from '../actions';
const { connect, connected, disconnect, disconnected } = ConnectionStage;
import { WampReducer } from './index';

export interface ReducerMap<S> {
  onConnected: (state: S, payload: autobahn.Session) => S;
  onDisconnected: (state: S) => S;
  onConnect?: (state: S) => S;
  onDisconnect?: (state: S) => S;
  onError?: (state: S, error: any) => S;
}

/** Helper class for managing a WAMP connection in redux.
 */
export default class ReduxWampConnection  {
  readonly type: string;
  readonly url: string;
  readonly realm: string;
  /** Action creator for a connect action. */
  readonly createConnectAction: () => ConnectionAction;
  /** Action creator for a disconnect action. */
  readonly createDisconnectAction: () => ConnectionAction;

  constructor(url: string, realm: string, type: string = 'WAMP_CONNECT') {
    this.type = type;
    this.url = url;
    this.realm = realm;
    this.createConnectAction = () => this.createAction(ConnectionStage.connect);
    this.createDisconnectAction = () => this.createAction(ConnectionStage.disconnect);

  }

  /** Helper for creating a set of redux reducer for a connection action.
   */
  createReducer<S>(
    onConnected: (state: S, payload: autobahn.Session) => S,
    onDisconnected: (state: S) => S,
    onConnect?: (state: S) => S,
    onDisconnect?: (state: S) => S,
    onError?: (state: S, error: string) => S)
    : WampReducer<S, autobahn.Session, ConnectionMeta> {
    return (state, action) => {
      const { error, payload, meta: { stage } } = action;
      if (error) {
        return onError ? onError(state, <string><any>payload) : state;
      }
      switch (stage) {
        case connected:
          return onConnected(state, payload);
        case disconnected:
          return onDisconnected(state);
        case connect:
          return onConnect ? onConnect(state) : state;
        case disconnect:
          return onDisconnect ? onDisconnect(state) : state;
        default:
          throw `unknown connection stage: ${stage}`;
      }
    };
  }

  /** Reducer */
  handleAction<S>(reducers: ReducerMap<S>, initialState: S = undefined): Reducer<S> {
    const { onConnected, onDisconnected, onConnect, onDisconnect, onError } = reducers;
    const wampReducer = this.createReducer(onConnected, onDisconnected, onConnect, onDisconnect, onError);

    return (state: S = initialState, action: Action) => action.type === this.type
      ? wampReducer(state, action as ConnectionAction)
      : state;
  }

  private createAction(stage: ConnectionStage): ConnectionAction {
    const { type, url, realm } = this;
    return {
      type: this.type,
      payload: null,
      meta: { url, realm, stage, role: Role.connection },
    };
  }
}
