import ReduxWampConnection from './connection';
import { ConnectionAction, Role, ConnectionStage } from '../actions';

test('sets data from constructor', () => {
  const connection = new ReduxWampConnection('ws://localhost:8080/wamp', 'test_1', 'WAMP_CONNECT');
  expect(connection.type).toBe('WAMP_CONNECT');
  expect(connection.url).toBe('ws://localhost:8080/wamp');
  expect(connection.realm).toBe('test_1');
});

test('creates expected connect action', () => {
  const connection = new ReduxWampConnection('ws://localhost:8080/wamp', 'test_1', 'WAMP_CONNECT');
  const action = connection.createConnectAction();
  const expected: ConnectionAction = {
    type: 'WAMP_CONNECT',
    payload: null,
    meta: {
      role: Role.connection,
      stage: ConnectionStage.connect,
      url: 'ws://localhost:8080/wamp',
      realm: 'test_1',
    },
  };
  expect(action).toEqual(expected);
});

test('creates expected disconnect action', () => {
  const connection = new ReduxWampConnection('ws://localhost:8080/wamp', 'test_1', 'WAMP_CONNECT');
  const action = connection.createDisconnectAction();
  const expected: ConnectionAction = {
    type: 'WAMP_CONNECT',
    payload: null,
    meta: {
      role: Role.connection,
      stage: ConnectionStage.disconnect,
      url: 'ws://localhost:8080/wamp',
      realm: 'test_1',
    },
  };
  expect(action).toEqual(expected);
});
