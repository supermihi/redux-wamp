import ReduxWampCaller from './call';
import { CallerAction, Role, CallerStage } from '../actions';

test('sets type & uri from constructor', () => {
  const caller = new ReduxWampCaller('CALL_TEST', 'com.example.call');
  expect(caller.type).toBe('CALL_TEST');
  expect(caller.uri).toBe('com.example.call');
});

test('creates expected call action', () => {
  const caller = new ReduxWampCaller<any, [number, string]>('CALL_TEST', 'com.example.call');
  const action = caller.createCallAction([1, 'two']);
  const expected: CallerAction<any, any> = {
    type: 'CALL_TEST',
    payload: null,
    meta: {
      role: Role.caller,
      stage: CallerStage.call,
      uri: 'com.example.call',
      args: [1, 'two'],
    },
  };
  expect(action).toEqual(expected);
});

test('can create empty-argument call action', () => {
  const caller = new ReduxWampCaller<string, null>('CALL_TEST', 'com.example.call');
  const action = caller.createCallAction();
  const expected: CallerAction<string, null> = {
    type: 'CALL_TEST',
    payload: null,
    meta: {
      role: Role.caller,
      stage: CallerStage.call,
      uri: 'com.example.call',
      args: null,
    },
  };
  expect(action).toEqual(expected);
});
