import { ReducerMeta } from 'redux-actions';
import { WampMeta } from '../actions';

export type WampReducer<S, Payload, Meta extends WampMeta> = ReducerMeta<S, Payload, Meta>;
