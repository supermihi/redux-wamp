# redux-wamp

[![pipeline status](https://gitlab.com/supermihi/redux-wamp/badges/master/pipeline.svg)](https://gitlab.com/supermihi/redux-wamp/commits/master)

Redux-wamp is a library that helps using the [autobahn.js](https://github.com/crossbario/autobahn-js) [WAMP](http://wamp-proto.org) library in a
[Redux](https://github.com/reactjs/redux)-App.

It comes with a Redux middleware that intercepts specific *WAMP actions* and a set of helpers to manage action creators and reducers for WAMP roles.

Currently, only the caller and subscriber roles are implemented. Feel free to submit merge requests for additional features!
